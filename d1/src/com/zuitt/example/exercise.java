package com.zuitt.example;

import java.util.Scanner;

public class exercise {

    public static void main(String[] args) {

        Scanner number = new Scanner(System.in);

        System.out.println("Enter a year:");
        int leapYear = number.nextInt();

        if (leapYear % 4 == 0)
        {
            System.out.println(leapYear + " is a leap year");
        }
        else
        {
            System.out.println(leapYear + " is not a Leap Year");
        }
    }
}
