package com.zuitt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class S2_A2 {

    public static void main(String[] args) {

        int [] primeNum = new int [5];

        primeNum[0] = 2;
        primeNum[1] = 3;
        primeNum[2] = 5;
        primeNum[3] = 7;
        primeNum[4] = 11;

        System.out.println("The first prime number is "+primeNum[0]);

        ArrayList<String> friends = new ArrayList<String>(Arrays.asList());

        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        System.out.println("My friends are: [" + friends.get(0) + ", " + friends.get(1) + ", " + friends.get(2) + ", " + friends.get(3) + "]");

        HashMap<String, Integer> inventory = new HashMap<String, Integer>();

        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);

        System.out.println("Our current inventory consists of: " + inventory);
    }
}
